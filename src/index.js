// ValueNet's 
import Made from './Made';
import madeConfigProvider from './configProvider';
import madeFileInput from './directives/fileInput';
import madeDropzone from './directives/dropzone';

angular
  .module( 'made-js', ['ngCookies'] )
  .config(['$cookiesProvider', function($cookiesProvider) {
    let host = window.location.hostname;
    let host_subparts = host.split('.')
    $cookiesProvider.defaults.secure = true;
    $cookiesProvider.defaults.path = '/';
    if (host_subparts.length >= 2) {
      let main_domain_name = host_subparts[host_subparts.length - 2];
      let ending = host_subparts[host_subparts.length - 1];
      $cookiesProvider.defaults.domain = '.' + main_domain_name + '.' + ending;
    } else {
      let main_domain_name = host_subparts[host_subparts.length - 1];
      if (main_domain_name !== 'localhost') {
        $cookiesProvider.defaults.domain = main_domain_name;
      } else {
        // secure everywhere except on localhost
        $cookiesProvider.defaults.secure = false;
      }
    }
    
  }])
  .provider( 'madeConfig', madeConfigProvider )
  .service( 'Made', Made )
  .directive( 'madeFileInput', madeFileInput )
  .directive( 'madeDropzone', madeDropzone );
